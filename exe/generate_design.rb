#!/usr/bin/env ruby -w
# frozen_string_literal: true

# require 'colorize'
# String.disable_colorization false
# require 'datafarming/error_handling'

require 'optparse'
require 'yaml'

require 'datafarming/scale_design'

DESIGN_TYPES = %i[resv ccd rotatable nolh fbd fbs].freeze
REQUIRED_SPECS = %i[min max].freeze
ELECTIVE_SPECS = %i[id decimals].freeze
LEVELS = [17, 33, 65, 129, 257, 512].freeze
TWO_PI = 2.0 * Math::PI

HELP_MSG = [
  'Generate, scale, stack, and replicate a variety of statistical designs',
  "of experiments. You are required to specify the desired '--design TYPE'.",
  '',
  "Usage: #{File.basename($PROGRAM_NAME)} [options] [optional factor specs]"
].freeze

EXTENDED_HELP = [
  "====================\n",
  "Using the '-k' option will specify the number of factors for a standardized",
  'design, in which all factors are normalized to the range of [-1, 1].',
  '',
  'Optional factor specifications can be used to create a design which scales',
  'the factors to individualized ranges. Specifications are then required for',
  'all factors, and the number of factors will be derived from the number of',
  "specifications provided (thus overriding the '-k' option). Each factor's",
  'specification is separated from the others by whitespace.',
  '',
  'Factor specifications contain two to four comma-separated components, and',
  'cannot include any whitespace. The format is:',
  "\n   id:STRING,min:NUMBER,max:NUMBER,decimals:INTEGER\n",
  "Components can be arranged in any order.  The components 'min' and 'max'",
  'are required, and define the range of experimentation for this factor.',
  "Components 'id' and 'decimals' are optional. If 'id' is omitted, id",
  "values will be generated for use with the '--headers' option. If 'decimals'",
  'is omitted, the factors values will be displayed with default precision.',
  ' '
].freeze

EXAMPLES = [
  "\n====================\n",
  'EXAMPLE 1:',
  "\n  #{File.basename($PROGRAM_NAME)} --design fbd -k 3 --no-headers",
  "\n  Generate a standardized frequency-based design with three factors,",
  '  without headers. All ranges are -1..1 and output has default precision.',
  '',
  'EXAMPLE 2:',
  "\n  #{File.basename($PROGRAM_NAME)} -d nolh id:Fred,min:-10,max:10 decimals:5,max:100,min:0",
  "\n  Generate a NOLH for two factors. The first factor will have a column",
  "  header of 'Fred', and values will be scaled in the range -10..10. The",
  "  second column will have an automatically generated header 'X_002', and",
  '  values will be in the range 0..100 with five decimal places of precision.',
  "  As described in '--verbose-help', factor specifications must be separated",
  "  by whitespace.",
  '',
  'EXAMPLE 3:',
  "\n  #{File.basename($PROGRAM_NAME)} -d nolh -k 5 --output_yaml > my_template.yaml",
  "\n  Generate a YAML template for a 5-factor NOLH design, and redirect it",
  "  to the file 'my_template.yaml'. (The output file can be given any name",
  '  of your choosing.) The resulting YAML file has an intuitive human',
  "  readable format. It can be edited using your favorite programmer's",
  '  text editor (NOTE: word processors will not work!). After saving your',
  '  revisions, you can read it back in to generate the actual design using',
  "  the '--input_yaml' option:\n",
  "  #{File.basename($PROGRAM_NAME)} --input_yaml < my_template.yaml",
  ' '
].freeze

def prog_help(opts)
  puts opts
  exit
end

def verbose(opts)
  puts opts
  puts EXTENDED_HELP.join("\n")
  exit
end

def examples(opts)
  puts opts
  puts EXAMPLES.join("\n")
  exit
end

def resv(options)
  require 'datafarming/res_v_seqs'
  raise 'Too many factors for ResV designs' if options[:num_factors] > RESV::INDEX.size

  RESV.make_design options[:num_factors]
end

def ccd(options)
  require 'datafarming/res_v_seqs'
  raise 'Too many factors for CCD designs' if options[:num_factors] > RESV::INDEX.size

  RESV.make_design(options[:num_factors]) + RESV.star_pts(options[:num_factors])
end

def rotatable(options)
  require 'datafarming/res_v_seqs'
  raise 'Too many factors for Rotatable designs' if options[:num_factors] > RESV::INDEX.size

  inv_len = 1.0 / Math.sqrt(options[:num_factors])
  RESV.make_design(options[:num_factors]).each do |line|
    line.map! { |value| value * inv_len }
  end + RESV.star_pts(options[:num_factors])
end

def nolh(options)
  require 'datafarming/nolh_designs'
  minimal_size =
    case options[:num_factors]
    when 1..7
      17
    when 8..11
      33
    when 12..16
      65
    when 17..22
      129
    when 23..29
      257
    when 30..100
      512
    else
      raise "invalid number of factors: #{options[:num_factors]}"
    end

  options[:levels] ||= minimal_size
  if options[:levels] < minimal_size
    raise "Latin hypercube with #{options[:levels]} levels is" \
          "too small for #{options[:num_factors]} factors."
  end

  NOLH::DESIGN_TABLE[options[:levels]]
end

def freqs2cols(options, design_set)
  raise "No design (yet) for #{options[:num_factors]} factors" unless
    design_set.key? options[:num_factors]

  design_num = -1 # the last one has largest min frequency

  ds = design_set[options[:num_factors]]
  freqs = ds.freqs[design_num]
  Array.new(freqs.size) do |row|
    omega = freqs[row]
    Array.new(ds.nyq) do |i|
      f = ((i * omega) % ds.nyq).to_f / ds.nyq
      Math.sin(f * TWO_PI)
    end
  end.transpose
end

def fbd(options)
  require 'datafarming/freq_sets'
  freqs2cols(options, FBD::DESIGN_SETS)
end

def fbs(options)
  require 'datafarming/screen_freq_sets'
  freqs2cols(options, FBS::DESIGN_SETS)
end

def validate_opts(options)
  required_options = [:design]
  missing = required_options - options.keys
  raise "Missing required options: #{missing}" unless missing.empty?

  options[:design] = options[:design].to_sym
end

def validate(spec)
  unknown = spec.keys - REQUIRED_SPECS - ELECTIVE_SPECS
  raise "Unknown factor spec(s): #{unknown}" unless unknown.empty?

  missing = REQUIRED_SPECS - spec.keys
  raise "Factor spec #{spec} missing #{missing}" unless missing.empty?
end

def optional_specs(factor_spec, i)
  YAML.safe_load("{#{factor_spec}}".gsub(':', ': '),
    permitted_classes: [Symbol]).tap do |spec|
    spec.transform_keys!(&:to_sym)
    spec[:decimals] ||= nil
    spec[:id] ||= "X_#{format('%03d', (i + 1))}"
    validate(spec)
  end
end

def parse_specs(options, remainder)
  if remainder.size.positive?
    options[:num_factors] = remainder.size
    remainder.map.with_index { |factor_spec, i| optional_specs(factor_spec, i) }
  elsif options.key? :num_factors
    Array.new(options[:num_factors]) do |i|
      { "id": "X_#{format('%03d', (i + 1))}", "min": -1, "max": 1, "decimals": nil }
    end
  else
    y_options, factor_specs = YAML.safe_load($stdin, permitted_classes: [Symbol])
    y_options.each_key { |key| options[key] ||= y_options[key] }
    factor_specs.each { |spec| validate(spec) }
  end
end

def parse_args
  options = {
    headers: true,
    replicate: 1,
    stack: 1,
    center: false
  }

  remainder = OptionParser.new do |opts|
    opts.banner = HELP_MSG.join("\n")
    opts.on('-h', '--help', 'Prints help') { prog_help(opts) }
    opts.on('-v', '--verbose-help', 'Print more verbose help') { verbose(opts) }
    opts.on('-x', '--examples', 'Show some examples') { examples(opts) }
    opts.on('-d', '--design=TYPE', DESIGN_TYPES,
            'REQUIRED: Type of design, one of:',
            "  #{DESIGN_TYPES.join ' '}")
    opts.on('-k', '--num_factors=QTY', /[1-9][0-9]*/, Integer,
            'Number of factors',
            'Ignored if optional factor specs provided')
    opts.on('-r', '--replicate=QTY', /[1-9][0-9]*/, Integer,
            'Replicate QTY times', '(default: 1)')
    opts.on('-s', '--stack=QTY', /[1-9][0-9]*/, Integer,
            'Stack QTY times', '(default: 1)')
    opts.on('--[no-]center', 'NOTE: Applies only to stacked designs',
            'Stacking includes center pt','(default: no-center)')
    opts.on('--[no-]headers', 'Output has column headers', '(default: headers)')
    opts.on('-l', '--levels=QTY', Regexp.new("(#{LEVELS.join(')|(')})"),
            Integer, 'NOTE: Applies only to NOLH designs',
            'Number of levels in the base NOLH:',
            "  #{LEVELS.join ' '}")
    opts.on('-o', '--output_yaml', 'Write design YAML to STDOUT')
    opts.on('-i', '--input_yaml', 'Read design YAML from STDIN')
  end.parse!(into: options)

  raise 'Optional factor specs ignored when using YAML input' if options[:input_yaml] && remainder.size.positive?

  specs = parse_specs(options, remainder)
  validate_opts options
  [options, specs]
end

def stack(design, num_stacks, centered)
  return design if num_stacks < 2
  raise "Stacking #{num_stacks} times exceeds number of design columns" if num_stacks > design[0].length

  num_stacks -= 1
  result = design.transpose
  if num_stacks > 0
    design_t = if centered
      design.transpose
    else
      design.delete_if {|row| row.all? &:zero? }.transpose
    end
    num_stacks.times do
      design_t.rotate!
      result.map!.with_index { |line, i| line + design_t[i] }
    end
  end
  result.transpose
end


ARGV << '--help' if ARGV.empty?

options, specs = parse_args

if options[:output_yaml]
  options.delete :output_yaml
  puts [options, specs].to_yaml
else
  base_design = method(options[:design]).call(options)
  separator = ','
  puts Array.new(options[:num_factors]) { |i| specs[i][:id] }.join(separator) if options[:headers]

  # stack
  design = stack(base_design, options[:stack], options[:center])
           .transpose.first(options[:num_factors]).transpose

  # scale
  scaler = specs.map do |spec|
    Scaler.new(min: spec[:min], max: spec[:max], decimals: spec[:decimals])
  end

  # replicate
  options[:replicate].times do
    design.each do |line|
      puts line.map.with_index { |v, i| scaler[i].scale(v) }.join(separator)
    end
  end
end
