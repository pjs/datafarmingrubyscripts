#!/usr/bin/env ruby -w

module CrossedDesigns
  # The "cross" method creates a large combinatorial design by crossing all
  # combinations of individual smaller designs.  It uses recursion to do so
  # because we don't know how many designs there may be in the input set.
  #
  # The method takes an array of arrays, where each sub-array contains a
  # single component design, and kicks off the recursive build process.
  def self.cross(inputs, idx = 0, tmp = [], solution = [])
    if idx >= inputs.size
      solution << tmp
    else
      inputs[idx].each { |dp| cross(inputs, idx + 1, tmp + dp, solution) }
    end
    solution
  end
end
