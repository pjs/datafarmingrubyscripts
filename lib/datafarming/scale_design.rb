#!/usr/bin/env ruby -w

# A Scaler object will rescale a design from standard range [-1, 1] to a
# range specified by min & max, with the desired number of decimals
class Scaler
  def initialize(min: -1, max: 1, decimals: nil)
    unless decimals.nil? || ((decimals >= 0) && (decimals == decimals.to_i))
      fail "Invalid decimals argument: #{decimals}. Must be a non-negative integer"
    end
    @range = (max - min).to_r / 2
    @mid = min + @range
    @scale_factor = (decimals.nil? || decimals.zero?) ? decimals : 10r**decimals
  end

  def scale(value)
    new_value = @mid + @range * value
    if @scale_factor.nil?
      if new_value.to_r.denominator == 1
        new_value.to_i
      else
        new_value.to_f
      end
    elsif @scale_factor.zero?
      new_value.round
    else
      ((@scale_factor * new_value).round / @scale_factor).to_f
    end
  end
end
