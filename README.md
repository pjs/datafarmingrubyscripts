### SETUP

This gem requires Ruby v3 or later to be installed on your system.

  - On MacOS, Ruby comes pre-installed.  Note: You can use the homebrew package manager from [brew.sh](https://brew.sh) if you want to install a newer version of Ruby which has numerous advantages over the one that currently ships with MacOS.
  
  - Windows users should use one of the installer packages available from [rubyinstaller.org](http://rubyinstaller.org/).  You will need to determine whether your have a 32 or 64 bit processor, and download the appropriate installer.  When running the installer, check the checkboxes to add the Ruby installation to your PATH and to associate `.rb` and `.rbw` files with Ruby. A terminal window will pop up during the installation—press "enter" to automatically install all of the required tools, and when prompted again (several minutes later) press "enter" again to end the installation.

You can check that Ruby is properly installed by opening `Terminal.app` (MacOS) or `CMD.EXE` (Windows) and typing `ruby --version` (note: two dashes).  You should see a response telling you which version of Ruby you are running.

After your Ruby installation is confirmed you can install the data farming Ruby scripts by running the following command.

    gem install datafarming

This assumes that you have a network connection. Additional dependencies will be installed automatically.  On MacOS or Linux systems, you may be required to use the `sudo` command to authenticate the installation.  In that case, type `sudo gem install datafarming` and enter your password when prompted.

Gem installation only needs to be done once.  If you explicitly downloaded the `.gem` file rather than using a network installation, you can delete it after performing the installation. If a newer version of the gem is released, you can upgrade to it with `gem update`.

### USAGE

Ruby is a powerful and concise object-oriented scripting language.  Normally, Ruby scripts are run from a command-line or terminal environment by typing the `ruby` command followed by a script name, often followed by one or more command-line arguments.  However, scripts installed as gems do not need the `ruby` command to be typed explicitly.  For example, typing `stripheaderdups.rb my_file.txt` will invoke the `stripheaderdups.rb` script and apply it to file `my_file.txt` in your current working directory.  Note that on Windows the `.rb` suffix is not needed to run your scripts, e.g., `stripheaderdups my_file.txt` will suffice.

All scripts in this distribution are self documenting if run with a `--help`, or `-h` option. You can run the command `datafarming.rb` at any point after installing this gem to see descriptions of the various data farming utilities included in thie distribution.

After installing the gem and making sure that gems are on your PATH, run the command `datafarming.rb` for a summary/overview of the various data farming tools.
